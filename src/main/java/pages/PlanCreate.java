package pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PlanCreate {
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath = "//div[@input-model = 'planningtemplates']/span/button")
	WebElement planTemplateButton;
	
	@FindBy(xpath = "//div[@input-model = 'teachers']/span/button")
	WebElement teacherLinkButton;
	
	@FindBy(xpath = "//div[@input-model = 'children']/span/button")
	WebElement childLinkButton;
	
	@FindBy(xpath = "//input[@data-ng-model='editPlan.title']")
	WebElement titleField;
	
	@FindBy(id = "summary_ifr")
	WebElement subjectField;
	
	@FindBy(id = "description_ifr")
	WebElement descriptionField;
	
	@FindBy(xpath = "//div[@data-ng-repeat='field in editPlan.template.customfields track by $index']//iframe")
	WebElement customField;
	
	
	@FindBy(xpath = "//a[@data-ng-click = 'uploadResourcesIframe()]")
	WebElement resourcesButton;
	
	@FindBy(xpath = "//span[@class = 'fileinput-button']")
	WebElement uploadpictureButton;
	
	@FindBy(xpath = "//button[@class = 'start']")
	WebElement startUploadButton;
	
	@FindBy(xpath = "//a[contains(text(), 'Finish')]")
	WebElement closeUploadScreenButton;
	
	@FindBy(xpath = "//a[contains(@data-ng-click, 'getPlans']") 
	WebElement linkToPlans;
	
	@FindBy(xpath = "//a[@title ='Save As Draft']")
	WebElement draftButton;
	
	@FindBy(xpath = "//a[contains(text(), 'Publish')]")
	WebElement publishButton;
	
	
	public PlanCreate(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void selectTemplate(String planName) {
		wait.until(ExpectedConditions.elementToBeClickable(planTemplateButton)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class = 'acol']//label//span[contains(text(), '" + planName + "')]"))).click();
	}
	
	public void linkToTeachers(String teachers) {
		wait.until(ExpectedConditions.elementToBeClickable(teacherLinkButton)).click();
		for(String s : teachers.split(",")) {
			driver.findElement(By.xpath("//div[@input-model = 'teachers']//span[contains(text(), '" + s + "')]")).click();
		}
	}
	
	public void linkToChildren(int[] children) {
		wait.until(ExpectedConditions.elementToBeClickable(childLinkButton)).click();
		for(int i = 0; i < children.length; i++) {
			driver.findElement(By.xpath("//div[@input-model = 'children']//span[contains(text(), 'Child " + children[i] + "')]")).click();
		}
	}
	
	public void enterTitle(String title) {
		titleField.sendKeys(title);
	}
	
	public void enterSubject(String summaryContent) throws InterruptedException {
		Thread.sleep(1000);
		//driver.switchTo().frame("summary_ifr");
		//Thread.sleep(1000);
		WebElement temp = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='summary_ifr']")));
		temp.click();
		System.out.println(summaryContent);
		temp.sendKeys(summaryContent);
		//driver.findElement(By.xpath("//*[@id='summary_ifr']")).sendKeys("test");
		//subjectField.sendKeys(summaryContent);
		//driver.switchTo().defaultContent();
	}
	
	public void enterDescription(String descriptionContent) {
		driver.switchTo().frame("description_ifr");
		descriptionField.sendKeys(descriptionContent);
		driver.switchTo().defaultContent();
	}
	
	public void enterCustomField(String customContent) {
		customField.sendKeys(customContent);
	}
	
	public void addResources(String filePath) throws InterruptedException, IOException {
		resourcesButton.click();
		driver.switchTo().frame("uploadFileIframe");
		Runtime.getRuntime().exec(filePath);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("uploadFileIframe");
		wait.until(ExpectedConditions.elementToBeClickable(startUploadButton)).click();
		wait.until(ExpectedConditions.elementToBeClickable(closeUploadScreenButton)).click();
		
	}
	
	public void linkToPlans(String plan) {
		wait.until(ExpectedConditions.elementToBeClickable(linkToPlans)).click();
		for(String s: plan.split(",")) {
			driver.findElement(By.xpath("//div[contains(text(), '" + s + "')]/..//label")).click();
		}
	}
	
	public void publishPlan() {
		wait.until(ExpectedConditions.elementToBeClickable(publishButton)).click();
	}
	
	public void draftPlan() {
		wait.until(ExpectedConditions.elementToBeClickable(draftButton)).click();
	}

	public void linkAllChildren() {
		wait.until(ExpectedConditions.elementToBeClickable(childLinkButton)).click();
		driver.findElement(By.xpath("//div[@input-model = 'children']//button[contains(text(), 'Select All')]")).click();
		childLinkButton.click();
	}

	public void linkAllTeachers() {
		wait.until(ExpectedConditions.elementToBeClickable(teacherLinkButton)).click();
		driver.findElement(By.xpath("//div[@input-model = 'teachers']//button[contains(text(), 'Select All')]")).click();
		teacherLinkButton.click();
	}
}
