package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PlanView {
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath = "//a[@title = 'All Plans']")
	WebElement allPlans;
	
	public PlanView(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void viewAllPlans() {
		wait.until(ExpectedConditions.elementToBeClickable(allPlans)).click();
	}
}
