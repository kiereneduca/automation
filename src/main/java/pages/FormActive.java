package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FormActive {
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath = "//a//span[contains(text(), 'New Form')]")
	//@FindBy(xpath = "//*[@id=\"page\"]/div[2]/div[2]/div/div[1]/div[1]/div[1]/div[2]/a[4]")
	WebElement createFormButton;
	
	@FindBy(xpath = "//a//span[contains(text(), 'New Template')]")
	WebElement createTemplateButton;
	
	WebElement archivedFormsButton;
	WebElement draftFormsButton;
	
	@FindBy(xpath = "//a[@title = 'All Templates']")
	WebElement templatesButton;
	
	@FindBy(xpath  = "//li[@id = 'kwick1']/a")
	WebElement messageBoardButton;
	
	@FindBy(xpath = "//a[@title = 'Click here to log out']")
	WebElement logoutButton;
	
	public FormActive(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void createForm() {
		wait.until(ExpectedConditions.elementToBeClickable(createFormButton)).click();
	}
	
	public void archivedForms() {
		
	}
	
	public void draftForms() {
		
	}
	
	public void templates() {
		wait.until(ExpectedConditions.elementToBeClickable(templatesButton)).click();	
	}
	
	public void messageBoard() {
		wait.until(ExpectedConditions.elementToBeClickable(messageBoardButton)).click();
	}
	
	public void logout() {
		wait.until(ExpectedConditions.elementToBeClickable(logoutButton)).click();
	}
	
	public void createTemplate() {
		wait.until(ExpectedConditions.elementToBeClickable(createTemplateButton)).click();
	}
}
