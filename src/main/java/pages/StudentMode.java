package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StudentMode {
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath = "//a[@title = 'Click here to log out']")
	WebElement logoutButton;
	
	@FindBy(xpath = "//span[contains(text(), 'Create Story')]")
	WebElement createStory;
	
	public StudentMode(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void createStory() {
		wait.until(ExpectedConditions.elementToBeClickable(createStory)).click();
	}
	
	
	public void logout() {
		wait.until(ExpectedConditions.elementToBeClickable(logoutButton)).click();
	}
}
