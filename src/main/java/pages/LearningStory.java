package pages;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.FileManipulation;
import utilities.Helper;


public class LearningStory {
	WebDriver driver;
	WebDriverWait wait;
	FileManipulation files;
	
	@FindBy(id = "Title")
	WebElement storyTitle;
	
	@FindBy(id = "WhenHappened")
	WebElement dateField;
	
	@FindBy(id = "tinymce")
	WebElement storyField;
	
	@FindBy(id = "tinymce")
	WebElement learningField;
	
	@FindBy(id = "tinymce")
	WebElement whatNextField;
	
	@FindBy(id = "authorComment")
	WebElement authorCommentField;
	
	@FindBy(id = "close_curriculum_links_window")
	WebElement curriculumClose;
	
	WebElement emailCheck;
	
	@FindBy(id = "save_button")
	WebElement publishButton;
	
	Helper random;
	
	public LearningStory(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
		files = new FileManipulation();
	}
	
	public void storyTitle(String title) {
		storyTitle.clear();
		storyTitle.sendKeys(title);
	}
	
	public void storyDate(String date) {
		dateField.clear();
		dateField.sendKeys(date);
	}
	
	public void addMedia(String mediaType) {
		if(mediaType.equalsIgnoreCase("PDF")) {
			
		} else if(mediaType.equalsIgnoreCase("Video")) {
			
		} else if(mediaType.equalsIgnoreCase("Photos")) {
			
		}
	}
	
	public void writeStory(String storyContent) {
		driver.switchTo().frame("Message_ifr");
		storyField.sendKeys(storyContent);
		driver.switchTo().defaultContent();
	}
	
	public void learningHappening(String content) {
		driver.switchTo().frame("WhatHere_ifr");
		learningField.sendKeys(content);
		driver.switchTo().defaultContent();
	}
	
	public void whatNext(String content) {
		driver.switchTo().frame("WhatNext_ifr");
		whatNextField.sendKeys(content);
		driver.switchTo().defaultContent();
	}
	
	//goals are ordered as: first entry is category,following entries are goals within category.
	public void curriculumGoals(String curriculum) throws IOException {
		random = new Helper(driver, wait);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), '" + curriculum + "' )]"))).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//select all curriculum links specified in config - WORKING
		/*for(int i = 1; i < 4; i++) {
			String[] goal = files.lookup("goals" + i).split(",");
			boolean firstLoop = true;
			for (String s : goal) {
				if(firstLoop) {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("curriculum-links-window")));
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), '" + s + "')]"))).click();
					firstLoop = false;
				} else {
					driver.findElement(By.xpath("//a[contains(text(), '" + s + "' )]//..//..//label")).click();
				}
			}
		}*/
		
		//select random curriculum links
		List<WebElement> goals = null;
		for(int i = 1; i < 4; i++) {
			String[] goal = files.lookup("goals" + i).split(",");
			boolean firstLoop = true;
			for (int j = 0; j <= random.number(1, 3); j++) {
				if(firstLoop) {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("curriculum-links-window")));
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(), '" + goal[0] + "')]")));
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), '" + goal[0] + "')]"))).click();
					firstLoop = false;
					goals = driver.findElements(By.xpath(".//div[@class='tab-content']//div[1]//ul//li"));
				} else {
					goals.get(j-1).findElement(By.xpath("//a[contains(text(), '" + goal[j] + "' )]//..//..//label")).click();
				}
			}
		}
		
		curriculumClose.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("curriculum-links-window")));
	}
	
	public void publishStory() {
		wait.until(ExpectedConditions.elementToBeClickable(publishButton)).click();
	}

	public void linkPlan() {
		
	}

	public void addAuthorComment(String comment) {
		authorCommentField.sendKeys(comment);
	}
	
}
