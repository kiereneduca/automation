package pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.FileManipulation;

public class Activate {
	
	WebDriver driver;
	WebDriverWait wait;
	FileManipulation files;
	
	@FindBy(id="Password")
	WebElement passwordField;
	
	@FindBy(id="ConfirmPassword")
	WebElement confirmPasswordField;
	
	@FindBy(xpath="//label[@class='left']")
	WebElement acceptCheckBox;

	@FindBy(id="vlidateForm")
	WebElement activateButton;
	
	@FindBy(xpath = "//a[@title = 'Click here to log out']")
	WebElement logoutButton;
	
	
	public Activate(WebDriver driver, WebDriverWait wait) {
		files = new FileManipulation();
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	//Enter username and password, then click login
	public void activateParent(String strPassword, String strConfPassword) throws IOException {
		passwordField.sendKeys(strPassword);
		confirmPasswordField.sendKeys(strConfPassword);
		wait.until(ExpectedConditions.elementToBeClickable(acceptCheckBox)).click();
		wait.until(ExpectedConditions.elementToBeClickable(activateButton)).click();
	}

	public void logout() {
		wait.until(ExpectedConditions.elementToBeClickable(logoutButton)).click();
	}
	
}
