package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MessageBoardCreate {
	WebDriver driver;
	WebDriverWait wait;
	
	public MessageBoardCreate(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait  = wait;
		PageFactory.initElements(driver, this);	
	}
	
	public void writeMessage(String storyContent) {
		driver.switchTo().frame("Message_ifr");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("tinymce"))).sendKeys(storyContent);
		driver.switchTo().defaultContent();
	}
	
	public void addTitle(String title) {
		WebElement temp = wait.until(ExpectedConditions.elementToBeClickable(By.id("Title")));
		temp.clear();
		temp.sendKeys(title);
	}
	
	public void addDate(String date) {
		WebElement temp = wait.until(ExpectedConditions.elementToBeClickable(By.id("WhenHappened")));
		temp.clear();
		temp.sendKeys(date);
	}
	
	public void publishMessage() {
		wait.until(ExpectedConditions.elementToBeClickable(By.id("save_button"))).click();
	}
}
