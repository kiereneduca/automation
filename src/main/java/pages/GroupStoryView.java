package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GroupStoryView {
	WebDriver driver;
	WebDriverWait wait;
	
	public GroupStoryView(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void createGroupStory() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), 'Create Group Story')]"))).click();
	}
	
	public void logout() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title = 'Click here to log out']"))).click();
	}

	public void clickGroupStory() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Create Group Story')]"))).click();
	}
}
