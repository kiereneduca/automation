package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Helper;


public class RoutinesCreate {
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath="//*[contains(@class, 'educa-nurs-icon-full')]")
	List<WebElement> routineTypes;
	
	WebElement childrenList;
	WebElement saveButton;
	
	public RoutinesCreate(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	//selects a random Routine type
	public void selectRandomType (int type, int[] children) {
		switch (type) {
		case 0:
			selectSleepType(children);
			break;
		case 1:
			selectFeedType(children);
			break;
		case 2:
			selectChangeType(children);
			break;
		case 3:
			selectNotesType(children);
			break;
		case 4:
			selectRequestType(children);
			break;
		}
	}
	
	public void selectSleepType (int[] children) {
		wait.until(ExpectedConditions.elementToBeClickable(routineTypes.get(0))).click();
		childrenList = driver.findElement(By.xpath("//div[@output-model = 'outputChildren']/span/button"));
		wait.until(ExpectedConditions.elementToBeClickable(childrenList)).click();
		linkChildren(children);
	}
	
	public void selectFeedType (int[] children) {
		wait.until(ExpectedConditions.elementToBeClickable(routineTypes.get(1))).click();
		childrenList = driver.findElement(By.xpath("//div[@output-model = 'outputChildren']/span/button"));
		wait.until(ExpectedConditions.elementToBeClickable(childrenList)).click();
		linkChildren(children);
	}
	
	public void selectChangeType (int[] children) {
		wait.until(ExpectedConditions.elementToBeClickable(routineTypes.get(2))).click();
		childrenList = driver.findElement(By.xpath("//div[@output-model = 'outputChildren']/span/button"));
		wait.until(ExpectedConditions.elementToBeClickable(childrenList)).click();
		linkChildren(children);
	}
	
	public void selectNotesType (int[] children) {
		wait.until(ExpectedConditions.elementToBeClickable(routineTypes.get(3))).click();
		childrenList = driver.findElement(By.xpath("//div[@output-model = 'outputChildren']/span/button"));
		wait.until(ExpectedConditions.elementToBeClickable(childrenList)).click();
		linkChildren(children);
	}
	
	public void selectRequestType (int[] children) {
		wait.until(ExpectedConditions.elementToBeClickable(routineTypes.get(4))).click();
		childrenList = driver.findElement(By.xpath("//div[@output-model = 'outputChildren']/span/button"));
		wait.until(ExpectedConditions.elementToBeClickable(childrenList)).click();
		linkChildren(children);
	}
	
	public void linkChildren(int[] children) {
		driver.findElement(By.xpath("//*[@id='childrenList']/span/div/div[1]/div[1]/button[1]")).click(); //Select all
		/*for(int i = 0; i < children.length; i++) {
			//Random chance to select child from list
			driver.findElement(By.xpath("//*[@id='childrenList']/span/div/div/div/div/label/span")).click();
		}*/
	}
	
	public void clickSave () {
		driver.findElement(By.xpath("//*[contains(text(), 'Save')]")).click();
	}
}
