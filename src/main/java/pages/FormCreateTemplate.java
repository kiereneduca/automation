package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.EventHandler;
import utilities.Helper;

public class FormCreateTemplate {
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;
	Helper random;
		
	@FindBy(xpath = "//div[@id = 'formType']//button")
	WebElement chooseFormType;
	
	@FindBy(id = "title")
	WebElement titleField;
	
	@FindBy(id = "tinymce")
	WebElement descriptionField;
	
	//Tracking Which section is currently selected
	String currentSection;
	int section;
	
	@FindBy(xpath = "//div[contains(text(), 'New Section')]/i")
	WebElement newSectionButton;
	boolean isFirstSection;
	
	@FindBy(xpath = "//input[@ng-model = 'updateCategory.name']")
	WebElement newSectionTitleField;
	
	@FindBy(id = "tinymce")
	WebElement newSectionDescriptionField;
	
	@FindBy(xpath = "//button[@ng-click = 'saveCategory(true)']")
	WebElement newSectionSaveButton;
	
	WebElement newQuestionButton;
	
	@FindBy(id = "tinymce")
	WebElement questionField;
	
	@FindBy(xpath = "//div[@on-item-click = 'fClickAnswerFormat( data )']//button")
	WebElement questionFormat;
	
	WebElement questionSaveButton;
	
	WebElement createTextBox;
	
	@FindBy(id = "allow-resources")
	WebElement allowResources;
	
	WebElement publishButton;
	
	
	public FormCreateTemplate(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
		isFirstSection = true;
		js = (JavascriptExecutor) driver;
		section = 0;
		random = new Helper(driver, wait);
	}
	
	public void changeFormType(String formType) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(chooseFormType)).click();
		driver.findElement(By.xpath("//div//label//span[contains(text(), '" + formType + "')]")).click();
	}
	
	public void addTitle(String titleContent) {
		titleField.sendKeys(titleContent);
	}
	
	public void addDescription(String descriptionContent) {
		descriptionField.sendKeys(descriptionContent);
	}
	
	public void addSection(String title, String description) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(newSectionButton)).click();
		Thread.sleep(1000);
		newSectionTitleField.sendKeys(title);
		driver.switchTo().frame("desc_ifr");
		newSectionDescriptionField.sendKeys(description);
		
		//select all text
		newSectionDescriptionField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		driver.switchTo().defaultContent();
		
		//generate a random number and change formatting according to number
		int num = random.number(1, 105);
		if(num % 3 == 0) 
			driver.findElement(By.xpath("//div[@id = 'add-category-modal']//div[@aria-label = 'Bold']")).click();
		if(num % 5 == 0)
			driver.findElement(By.xpath("//div[@id = 'add-category-modal']//div[@aria-label = 'Italic']")).click();
		if(num % 7 == 0)
			driver.findElement(By.xpath("//div[@id = 'add-category-modal']//div[@aria-label = 'Underline']")).click();
		
		newSectionSaveButton.click();
		section++;
		Thread.sleep(1000);
		//change the reference for dynamic elements
		newQuestionButton = driver.findElement(By.xpath("//div[contains(@class, 'tab-pane')][" + section + "]/a[contains(text(), 'Add Question')]"));
		createTextBox = driver.findElement(By.xpath("//div[contains(@class, 'tab-pane')][" + section + "]/div[contains(text(), 'Add Additional Text Box')]"));
		
		if(isFirstSection)
			newSectionButton = driver.findElement(By.xpath("//li[@title = 'add new section']/i"));
	}
	

	public void newQuestion(String question, String questionType) throws InterruptedException {
		
		Thread.sleep(2000);
		
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, 'tab-pane')][\" + section + \"]/a[contains(text(), 'Add Question')]"))).click();
		driver.findElement(By.xpath("//div[contains(@class, 'tab-pane')][" + section + "]/a[contains(text(), 'Add Question')]")).click();
		Thread.sleep(1000);
		//wait.until(ExpectedConditions.elementToBeClickable(newQuestionButton)).click();
		driver.switchTo().frame("milestone-question_ifr");
		questionField.clear();
		questionField.sendKeys(question);
		questionField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		driver.switchTo().defaultContent();
		
		//Random formatting
		/*int num = random.number(1, 105);
		if(num % 3 == 0) 
			driver.findElement(By.xpath("//div[@id = 'add-question-modal']//div[@aria-label = 'Bold']")).click();
		if(num % 5 == 0)
			driver.findElement(By.xpath("//div[@id = 'add-question-modal']//div[@aria-label = 'Italic']")).click();
		if(num % 7 == 0)
			driver.findElement(By.xpath("//div[@id = 'add-question-modal']//div[@aria-label = 'Underline']")).click();
		*/
		//Choose question type
		questionFormat.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div//span[contains(text(), '" + questionType + "')]"))).click();
		//save question
		driver.findElement(By.xpath("//button[@ng-click = 'saveQuestion(true)']")).click();
	}
	
	public void addTextBox() throws InterruptedException {
		Thread.sleep(2000);
		createTextBox = driver.findElement(By.xpath("//div[contains(@class, 'tab-pane')][" + section + "]/div[contains(text(), 'Add Additional Text Box')]"));
		wait.until(ExpectedConditions.elementToBeClickable(createTextBox)).click();
		
	}
	
	public void allowResources() {
		allowResources.click();
	}
	
	public void publishForm() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title = 'Publish']"))).click();
		section = 0;
	}
}
