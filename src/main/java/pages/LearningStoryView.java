package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LearningStoryView {
	WebDriver driver;
	WebDriverWait wait;
	
	
	public LearningStoryView(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void returnToChildren() {
		wait.until(ExpectedConditions.elementToBeClickable((By.id("kwick4")))).click();
	}
}
