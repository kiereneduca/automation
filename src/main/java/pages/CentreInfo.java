package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

// OUTDATED - plans, forms moved under Tools etc.
public class CentreInfo {
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath = "//a//span[contains(text(), 'Planning')]")
	WebElement planningButton;
	
	@FindBy(xpath = "//a//span[contains(text(), 'Forms')]")
	WebElement formsButton;
	
	public CentreInfo(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void clickPlanning() {
		wait.until(ExpectedConditions.elementToBeClickable(planningButton)).click();
	}
	
	public void clickForms() {
		wait.until(ExpectedConditions.elementToBeClickable(formsButton)).click();
	}
}
