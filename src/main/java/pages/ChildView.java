package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChildView {
	WebDriver driver;
	WebDriverWait wait;
	
	
	public ChildView(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void returnToChildren() throws InterruptedException {
		Thread.sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("kwick4"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"kwick4\"]/ul/li[1]/a"))).click();
	}
	
	public void createStory() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), 'Create Story')]"))).click();
	}
}
