package pages;

import java.io.IOException;
import java.util.List;
import utilities.Helper;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FormCreate {
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;
	Helper random;
	
	List<WebElement> forms;
	
	@FindBy(xpath = "//div[@id = 'selectFormTemplate']/span/button")
	WebElement selectFormTemplate;
	
	@FindBy(xpath = "//div[@output-model = 'linkedTeachers']/span/button")
	WebElement linkToTeachers;
	
	@FindBy(xpath = "//div[@output-model = 'subjectChildren']/span/button")
	WebElement whoIsThisFormFor;
	
	@FindBy(xpath = "//input[@name='title']")
	WebElement titleField;
	
	@FindBy(id = "tinymce")
	WebElement summaryField;
	
	@FindBy(xpath = "//ul[@class = 'nav nav-tabs']")
	WebElement sectionTab;
	
	WebElement addResourceButton;
	
	@FindBy(xpath = "//span[@class = 'fileinput-button']")
	WebElement uploadpictureButton;
	
	@FindBy(xpath = "")
	WebElement startUploadButton;
	
	@FindBy
	WebElement closeUploadScreenButton;
	
	@FindBy(xpath = "//a[@title= 'Publish']")
	WebElement publishButton;
	
	@FindBy(xpath = "//a[@title= 'Save As Draft']")
	WebElement draftButton;
	
	public FormCreate(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		js = (JavascriptExecutor) driver;
		PageFactory.initElements(driver, this);
		this.wait = wait;
		random = new Helper(driver, wait);
	}
	
	public void selectForm(String formName) {
		
		wait.until(ExpectedConditions.elementToBeClickable(selectFormTemplate)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id = 'selectFormTemplate']//span[contains(text(), '" + formName + "')]"))).click();
	}
	
	public void chooseFormPerson(String personName) {
		wait.until(ExpectedConditions.elementToBeClickable(whoIsThisFormFor)).click();
		if(whoIsThisFormFor.findElements(By.xpath("//div[@output-model = 'subjectTeachers']")).size() > 0) {
			teacherForm(personName);
		} else if(whoIsThisFormFor.findElements(By.xpath("//div[@output-model = 'subjectChildren']")).size() > 0) {
			childForm(personName);
		}
	}
	
	public void teacherForm(String teacher) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div//span[contains(text(), '" + teacher + "')]"))).click();
		whoIsThisFormFor.click();
	}
	
	public void childForm(String child) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div//span[contains(text(), '" + child + "')]"))).click();
		whoIsThisFormFor.click();
	}
	
	public void addTitle(String titleContent) {
		titleField.sendKeys(titleContent);
		
	}
	
	public void addSummary(String summaryContent) {
		driver.switchTo().frame("summary_ifr");
		summaryField.sendKeys(summaryContent);
		summaryField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		driver.switchTo().defaultContent();
		/*int num = random.number(1, 105);
		if(num % 3 == 0) 
			driver.findElement(By.xpath("//div[@data-ng-show = 'selectFormTemplate']//div[@aria-label = 'Bold']")).click();
		if(num % 5 == 0)
			driver.findElement(By.xpath("//div[@data-ng-show = 'selectFormTemplate']//div[@aria-label = 'Italic']")).click();
		if(num % 7 == 0)
			driver.findElement(By.xpath("//div[@data-ng-show = 'selectFormTemplate']//div[@aria-label = 'Underline']")).click();			
		*/
	}
	
	public void answerQuestions(String answer) {
		
		List<WebElement> questions = driver.findElements(By.xpath("//div[contains(@class, 'active')]/div[contains(@data-ng-repeat, 'category.questions')]"));
		
		for(WebElement q : questions) {
			//If question is slider
			if(q.findElements(By.xpath(".//input[contains(@class, 'slider')]")).size() > 0) {
				WebElement slider = q.findElement(By.xpath(".//div[contains(@class, 'jslider-value')]"));
				js.executeScript("arguments[0].innerHTML = 4", slider);
			} 
			//If question is single choice
			else if(q.findElements(By.xpath(".//div[contains(@data-ng-if, 'single choice')]")).size() > 0) {
				q.findElements(By.xpath(".//label")).get((int)(Math.random() * 4) % 2 ).click();
			} 
			//If question is multi choice
			else if(q.findElements(By.xpath(".//div[contains(@data-ng-if, 'multiple choice')]")).size() > 0) {
				q.findElements(By.xpath(".//label")).get((int)(Math.random() * 4) % 2 ).click();
			}
			//if there is a text box
			if(q.findElements(By.xpath(".//textarea[contains(@data-ng-if, 'text box')]")).size() > 0) {
				q.findElement(By.xpath(".//textarea[contains(@data-ng-if, 'text box')]")).sendKeys(answer);
			}	

		}
	}
	public int getSections() {
		return sectionTab.findElements(By.xpath(".//a")).size();
		
	}
	
	public void switchSection(int sectionNum) {
		sectionTab.findElements(By.xpath(".//a")).get(sectionNum).click();
	}
	
	public void addResource(String filePath) throws InterruptedException, IOException {
		addResourceButton.click();
		driver.switchTo().frame("uploadFileIframe");
		Thread.sleep(2000);
		Runtime.getRuntime().exec(filePath);
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		driver.switchTo().frame("uploadFileIframe");
		startUploadButton.click();
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("scroll(0,250);");
		closeUploadScreenButton.click();
	}
	
	public void draftForm() {
		draftButton.click();
	}
	
	public void publishForm() {
		wait.until(ExpectedConditions.elementToBeClickable(publishButton)).click();
	}
}
