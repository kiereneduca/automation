package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PlanActive {
	
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath = "//a//span[contains(text(), 'Create Plan')]")
	WebElement createPlanButton;
	
	@FindBy(xpath = "//a//span[contains(text(), 'Create Template')]")
	WebElement templateButton;
	
	@FindBy(xpath = "//div//a[contains(text(), 'Templates')]")
	WebElement templatesTabButton;
	
	@FindBy(xpath = "//a//span[contains(text(), 'Draft Plans')]")
	WebElement draftPlansButton;
	
	@FindBy(xpath = "//a//span[contains(text(), 'Archived Plans')]")
	WebElement archivedPlansButton;
	
	@FindBy(xpath = "//div//li[contains(text(), 'Logout')]")
	WebElement logoutButton;
	
	//Copy/Archive buttons for individual plans
	WebElement archivePlanOption;
	WebElement copyPlanOption;
	
	public PlanActive(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void createPlan() {
		wait.until(ExpectedConditions.elementToBeClickable(createPlanButton)).click();
	}
	
	public void draftPlans() {
		wait.until(ExpectedConditions.elementToBeClickable(draftPlansButton)).click();
	}
	
	public void archivedPlans() {
		wait.until(ExpectedConditions.elementToBeClickable(archivedPlansButton)).click();
	}
	
	public void planTemplate() {
		wait.until(ExpectedConditions.elementToBeClickable(templateButton)).click();
	}
	
	public void clickPlanTemplate() {
		wait.until(ExpectedConditions.elementToBeClickable(templateButton)).click();
	}
	
	public void clickTemplates() {
		wait.until(ExpectedConditions.elementToBeClickable(templatesTabButton)).click();
	}
	
	public void logout() {
		logoutButton.click();
	}
}
