package pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.FileManipulation;

public class Login {
	
	WebDriver driver;
	WebDriverWait wait;
	FileManipulation files;
	//WebDriver driver;
	
	@FindBy(id="email")
	WebElement emailField;
	
	@FindBy(id="password")
	WebElement passwordField;

	@FindBy(id="vlidateForm")
	WebElement loginButton;
	

	
	public Login(WebDriver driver, WebDriverWait wait) {
		files = new FileManipulation();
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	//Enter username and password, then click login
	public void login(String strUsername, String strPassword) throws IOException {
		emailField.sendKeys(files.lookup(strUsername));
		passwordField.sendKeys(files.lookup(strPassword));
		loginButton.click();
	}
	
}
