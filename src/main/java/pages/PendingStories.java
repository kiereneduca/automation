package pages;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.FileManipulation;
import utilities.Helper;


public class PendingStories {
	WebDriver driver;
	WebDriverWait wait;
	FileManipulation files;
	
	@FindBy(xpath = "//span[contains(text(), 'Approve')]")
	WebElement approveButton;
	
	@FindBy(xpath = "//span[contains(text(), 'Rework')]")
	WebElement reworkButton;
	
	@FindBy(xpath = "//button//span[contains(text(), 'Approve')]")
	WebElement modalApproveButton;

	
	public PendingStories(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
		files = new FileManipulation();
	}

	public void approveStory() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), 'Approve')]"))).click();
	}
	
	public void reworkStory() {
		wait.until(ExpectedConditions.elementToBeClickable(reworkButton)).click();
	}

	public void addComment(String comment) {
		driver.switchTo().frame("Approve_ifr");
		WebElement e = wait.until(ExpectedConditions.elementToBeClickable(By.id("tinymce")));
		e.click();
		e.sendKeys(comment);
		driver.switchTo().defaultContent();
	}

	public void clickApprove() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(modalApproveButton)).click();
		Thread.sleep(2000);
	}
	
}
