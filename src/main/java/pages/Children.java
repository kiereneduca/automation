package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Children {
	
	
	WebDriver driver;
	WebDriverWait wait;
	
	
	@FindBy(xpath="//div[@id='widepage']//span[contains(text(),'Pending Stories')]")
	WebElement pendingStoriesButton;
	
	WebElement childProfilePhoto;
		
	public Children(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
		
	}
	
	public void createChild() {
		wait.until(ExpectedConditions.elementToBeClickable(By.id("add_new_child"))).click();
	}

	public void clickChild(String childName){
		childProfilePhoto = driver.findElement(By.xpath("//img[contains(@title, '" + childName + "')]"));
		wait.until(ExpectedConditions.elementToBeClickable(childProfilePhoto));
	}
	
	public void clickGroupStory(){
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Create Group Story')]"))).click();
	}
	
	public void clickPendingStories() {
		wait.until(ExpectedConditions.elementToBeClickable(pendingStoriesButton));
	}
	
	public void createStory(String child) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), '" + child + "')]/ancestor::ul//a[contains(text(), 'Create Story')]"))).click();
	}

	public void clickFirstChild() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//img[@class='photo'])[1]"))).click();
	}
	
}