package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FormTemplate {
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath = "//a//span[contains(text(), 'Create Template')]")
	WebElement createTemplateButton;
	
	
	WebElement messageBoard;
	
	public FormTemplate(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait= wait;
		PageFactory.initElements(driver, this);
	}
	

	public void createTemplates() {
		createTemplateButton.click();
	}
	
	public void messageBoard() {
		messageBoard = driver.findElement(By.xpath("//li[@id = 'kwick1']/a"));
		messageBoard.click();
	}
}
