package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PlanTemplate {
	
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath = "//a//span[contains(text(), 'Create Template')]")
	WebElement createTemplateButton;
	
	@FindBy(xpath = "//*[@id=\"planningtemplates-list-table-data\"]/div[1]/div[2]/table/tbody/tr[2]/td[6]/a")
	WebElement copyTemplateButton;
	 
	
	public PlanTemplate(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void createTemplate() {
		wait.until(ExpectedConditions.elementToBeClickable(createTemplateButton)).click();
	}

	public void copyTemplate() {
		wait.until(ExpectedConditions.elementToBeClickable(copyTemplateButton)).click();
	}
}
