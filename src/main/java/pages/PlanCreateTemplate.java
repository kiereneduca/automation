package pages;

import java.time.LocalDate;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PlanCreateTemplate {
	
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy
	WebElement publishButton;
	
	public PlanCreateTemplate(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}

	public void enterTitle(String title) {
		WebElement temp = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"managetemplate\"]/div[2]/input")));
		temp.click();
		temp.sendKeys(LocalDate.now()+" - "+title);
	}

	public void addField(String title, String text) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"managetemplate\"]/div[10]/a"))).click();
		WebElement temp = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"customfieldmodal\"]/div[3]/input")));
		temp.click();
		temp.sendKeys(title);
		WebElement desc = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"customfieldmodal\"]/div[4]/textarea")));
		desc.click();
		desc.sendKeys(text);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"customfieldmodal\"]/div[5]/div[3]/label"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"customfieldmodal\"]/div[6]/button"))).click();
	}

	public void publishPlan() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title = 'Publish']"))).click();
	}

}
