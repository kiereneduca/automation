package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Helper;

public class ChildCreate {
	WebDriver driver;
	WebDriverWait wait;
	Helper random;
	
	WebElement firstName;
	WebElement lastName;
	WebElement dateOfBirthField;
	WebElement startDateField;
	WebElement saveButton;
	
	public ChildCreate(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
		random = new Helper(driver, wait);
	}
	
	public void enterName(String first, String last) {
		wait.until(ExpectedConditions.elementToBeClickable(By.id("FirstName"))).sendKeys(first);
		driver.findElement(By.id("LastName")).sendKeys(last);
	}
	
	public void selectGender() {
		double gender = Math.random();
		//small chance for no gender specified
		if(Math.random() < 0.1)
			return;
		if(gender < 0.55)
			driver.findElement(By.xpath("//label[@for = 'boy']")).click();
		else
			driver.findElement(By.xpath("//label[@for = 'girl']")).click();
	}
	
	public void dateOfBirth(String dateOfBirth) {
		dateOfBirthField = driver.findElement(By.id("DateOfBirth"));
		dateOfBirthField.clear();
		dateOfBirthField.sendKeys(dateOfBirth);
	}
	
	public void startDate(String startDate) {
		startDateField = driver.findElement(By.id("StartDate"));
		startDateField.clear();
		startDateField.sendKeys(startDate);
	}
	
	public int getClasses() {
		return driver.findElements(By.xpath("//select[@id = 'classIds']/option")).size();
	}
	
	public void pickClass(int classNum) {
		driver.findElement(By.xpath("//select[@id = 'classIds']/option[" + classNum + "]")).click();
		
	}
	
	public void saveChild() {
		wait.until(ExpectedConditions.elementToBeClickable(By.id("save_button"))).click();
	}
}
