package pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.FileManipulation;

public class GroupStory {

	WebDriver driver;
	WebDriverWait wait;
	FileManipulation files;
	
	@FindBy(xpath = "//*[@id=\"sgs\"]/div[2]/div[1]/span/button/span[2]")
	WebElement classList;
	
	@FindBy(xpath = "//*[@id=\"sgs\"]/div[2]/div[2]/span/button/span[2]")
	WebElement childList;

	@FindBy(id = "Title")
	WebElement storyTitle;
	
	@FindBy(id = "WhenHappened")
	WebElement dateField;
	
	@FindBy(id = "tinymce")
	WebElement storyTextField;
	


	@FindBy(xpath="//form[@id='upload_image_form']//div[@id='upload_file_form_control']")
	WebElement uploadFileButton;
	
	@FindBy(xpath="//form[@id='upload_image_form']//div[@id='upload_file_form_control']")
	WebElement addVideoButton;
	
	@FindBy(xpath="//form[@id='fileupload']//table//button//span[contains(text(),'Start')]")
	WebElement startUploadVideoButton;	
	
	@FindBy(xpath="//form[@id='fileupload']//table/tbody/tr/td/span[contains(text(),'Error')]")
	WebElement errorVideoFile;
	
	@FindBy(xpath="//form[@id='fileupload']//table/tbody/tr/td/button/span[contains(text(),'Cancel')]")
	WebElement errorCancelButton;
	
	//the delete button is used for both photo and video - to verify if a video has been uploaded successfully
	@FindBy(xpath = "//form[@id='fileupload']//table/tbody/tr/td[contains(@class,'delete')]")
	WebElement deleteButton;

	@FindBy(xpath = "//a[@id='open_multi_photo_upload']//span[contains(text(),'Add Photos')]")
	WebElement uploadPhotoButton;

	@FindBy(id = "upload_image_form_image")
	WebElement addPhotoButton;

	@FindBy(xpath = "//form[@id='fileupload']//span[contains(text(),'Start upload')]")
	WebElement startUploadButton;

	@FindBy(xpath = "//a[@id='close_multi_photo_upload']")
	WebElement finishButton;
	
	@FindBy(xpath="//form[@id='fileupload']//table/tbody/tr/td/span[contains(text(),'Error')]")
	WebElement errorPhoto;

	@FindBy(id = "close_curriculum_links_window")
	WebElement curriculumClose;
	//TODO
	@FindBy(xpath = "//button[contains(text(),'Select Plan(s)')]")
	WebElement planButton;
	
	@FindBy(xpath = "//span[contains(text(),'link to Yuonne Hise - 2')]")
	WebElement oblivionGroupPlan;

	//TODO
	@FindBy(xpath = "//span[contains(text(),'reading improvement plan')]")
	WebElement selectPlan;

	@FindBy(id = "//button//span[contains(text(),'Draft')]")
	WebElement saveDraftButton;
	
	@FindBy(xpath = "//button[@id='save_button']")
	WebElement publishStory;
	
	@FindBy(xpath = "//button//span[contains(text(),'Submit for Approval')]")
	WebElement submitForApproval;
	
	@FindBy(xpath="//div[@id='main_body']/fieldset//div//a[contains(text(),'Remove')]")
	WebElement removeFile;

	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	WebElement logoutButton;

	public GroupStory(WebDriver driver, WebDriverWait wait) {
		this.wait = wait;
		this.driver = driver;
		PageFactory.initElements(driver, this);
		files = new FileManipulation();
	}

	public void selectClass(String classes) {
		wait.until(ExpectedConditions.elementToBeClickable(classList)).click();
		for(String s : classes.split(",")) {
			driver.findElement(By.xpath("//ul//span[contains(text(), '" + s + "')]")).click();
		}
	}
	
	//select children using a list of numbers - Anubis HQ site uses simple Child 1, Child 2 system.
	public void selectChildren(int[] children) {
		wait.until(ExpectedConditions.elementToBeClickable(childList)).click();
		for(int i = 0; i < children.length; i++) {
			//Random chance to select child from list
			driver.findElement(By.xpath("//ul//span[contains(text(), 'Child " + children[i] + "')]")).click();
		}
	}
	
	
	public void enterTitle(String title) {
		storyTitle.clear();
		storyTitle.sendKeys(title);
	}
	
	public void enterDate(String date) {
	dateField.clear();
	dateField.sendKeys(date);
	}
	
	//fill in story content
	public void enterStory(String storyContent) {
		driver.switchTo().frame("Message_ifr");
		storyTextField.sendKeys(storyContent);
		driver.switchTo().defaultContent();
		
	}

	//fill in optional question 1
	public void learningHappening(String question1) {
		driver.switchTo().frame("WhatHere_ifr");
		storyTextField.click();
		storyTextField.sendKeys(question1);
		driver.switchTo().defaultContent();
	}
	
	//fill in optional question 2
	public void whatNext(String question2) {
		driver.switchTo().frame("WhatNext_ifr");
		storyTextField.click();
		storyTextField.sendKeys(question2);
		driver.switchTo().defaultContent();
	}
	
	//select curriculum goals from config file - first entry is category name, following entries are goal numaes
	public void curriculumGoals(String curriculum) throws IOException {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), '" + curriculum + "' )]"))).click();
		for(int i = 1; i < 4; i++) {
			String[] goal = files.lookup("goal" + i).split(",");
			boolean firstLoop = true;
			for (String s : goal) {
				if(firstLoop) {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), '" + s + "')]"))).click();
					firstLoop = false;
				} else {
					driver.findElement(By.xpath("//a[contains(text(), '" + s + "')]/../..//label")).click();
				}
			}
		}
		curriculumClose.click();
	}

	public void selectPlan() {		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", planButton);		
		planButton.click();
		selectPlan.click();
	}
	
	public void addResources() {
		
	}

	public void saveDraft() {
		saveDraftButton.click();
	}

	public void publishStory() {
		wait.until(ExpectedConditions.elementToBeClickable(publishStory)).click();
	}
	
	public void logout() {
		logoutButton.click();
	}
}
