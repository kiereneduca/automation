package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MessageBoard {
	
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(xpath="//li[@id='kwick4']/a")
	WebElement childrenTopMenu;
	
	@FindBy(xpath="//*[@id=\"kwick4\"]/ul/li[1]")
	WebElement allChildrenTopMenu;
	
	@FindBy(id="testID")
	WebElement textOnTopLeft;
	
	
	@FindBy(id="toolmailboxicon")
	WebElement bellIcon ;
	
	@FindBy(xpath="//a[@class='educa-tool-bar-link']/span[contains(text(),'My Stories')]")
	WebElement myStoriesIcon;
	
	@FindBy(xpath="//div[@id='page']//li/a[contains(text(),'Logout')]")
	WebElement logout;
	
	@FindBy(xpath="//div[@id='page']//span[contains(text(),'Create Parent Story')]")
	WebElement createParentStoryButton;

	@FindBy(xpath="//a[@title = 'Create Message']")
	WebElement createMessageButton;
	
	@FindBy(xpath = "//li[@id = 'kwick2']/a")
	WebElement centreButton;
	
	@FindBy(xpath = "//li[@id = 'kwick2']//a[contains(text(), 'About Us')]")
	WebElement aboutUsButton;
	
	@FindBy(xpath = "//*[@id=\"kwick6\"]/a")
	WebElement toolsButton;
	
	@FindBy(xpath = "//*[@id=\"kwick6\"]/ul/li[1]")
	WebElement planningButton;
	
	@FindBy(xpath = "//a[contains(text(), 'Forms')]")
	WebElement formsButton;
	
	@FindBy(xpath = "//a[contains(text(), 'Routines')]")
	WebElement routinesButton;
	
	@FindBy(xpath = "//a[contains(text(), 'Pending Stories')]")
	WebElement pendingStoriesButton;
	
	public MessageBoard(WebDriver driver, WebDriverWait wait)
	{
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	//click Children on top menu
	public void clickChildren() {
		wait.until(ExpectedConditions.elementToBeClickable(childrenTopMenu)).click();
	}
	
	public void clickAllChildren() {
		wait.until(ExpectedConditions.elementToBeClickable(allChildrenTopMenu)).click();
	}
	
	//get text from the Message Board
	public String messageBoard() {
		return textOnTopLeft.getText();
	}	
	
	public void clickBellIcon() {
		wait.until(ExpectedConditions.elementToBeClickable(bellIcon)).click();			
	}		
		
	public void clickNotification(String storyTitle) {
		
		//click on notification
		driver.findElement(By.xpath("//ul[@id='mail-holder']/li//span[contains(text(),'"+ storyTitle +"')]" )).click();
		
		//click on link inside notification message
		driver.findElement(By.xpath("//a[contains(text(),'" + storyTitle + "')]")).click();		
		
	}	
	
	public void clickMyStoriesIcon() {
		myStoriesIcon.click();		
	}
	
	public void createParentStory() {
		createParentStoryButton.click();
	}
	
	public void createMessage() {
		createMessageButton.click();
	}
	
	public void clickAboutUs() {
		wait.until(ExpectedConditions.elementToBeClickable(centreButton)).click();
		wait.until(ExpectedConditions.elementToBeClickable(aboutUsButton)).click();
	}
	
	public void clickLogout() {
		wait.until(ExpectedConditions.elementToBeClickable(logout)).click();	
	}
	
	public void clickTools() {
		wait.until(ExpectedConditions.elementToBeClickable(toolsButton)).click();	
	}
	
	public void clickPlanning() {
		wait.until(ExpectedConditions.elementToBeClickable(planningButton)).click();	
	}

	public void clickForms() {
		wait.until(ExpectedConditions.elementToBeClickable(formsButton)).click();
	}
	
	public void clickRoutines() {
		wait.until(ExpectedConditions.elementToBeClickable(routinesButton)).click();
	}
	
	public void clickPendingStories() {
		wait.until(ExpectedConditions.elementToBeClickable(pendingStoriesButton)).click();
	}
}
