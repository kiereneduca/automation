package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FormArchive {
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy
	WebElement createFormButton;
	WebElement activeFormsButton;
	WebElement draftFormsButton;
	WebElement templatesButton;
	
	public FormArchive(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}
	
	public void createForm() {
		
	}
	
	public void activeForms() {
		
	}
	
	public void draftForms() {
		
	}
	
	public void templates() {
		
	}
}
