package controllers;

import java.io.IOException;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import utilities.FileManipulation;

public class DriverInit {
	
	private static DriverInit instanceDriver = null;
	private WebDriver driver;
	FileManipulation files = new FileManipulation();
	
	private DriverInit() {
	}
	
	public WebDriver firefox() throws IOException {
		System.setProperty("webdriver.gecko.driver", files.lookup("firefox"));
				
		//System.setProperty("webdriver.gecko.driver", "D:\\Downloads\\geckodriver-v0.19.1-win64\\geckodriver.exe");
        driver = new FirefoxDriver();
		driver.manage().window().maximize();
		return driver;
	}
	
	public WebDriver chrome() throws IOException {
		System.setProperty("webdriver.chrome.driver", files.lookup("chrome"));
		//System.setProperty("webdriver.chrome.driver", "D:\\Downloads\\chromedriver_win32\\chromedriver.exe");
        //driver = new ChromeDriver();
		//Set up console logging
		ChromeOptions caps = new ChromeOptions();
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.ALL);
        caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        driver = new ChromeDriver(caps);
		
        driver.manage().window().maximize();
		return driver;
	}
	
	public WebDriver ie() throws IOException {
		System.setProperty("webdriver.ie.driver", files.lookup("ie"));
		//System.setProperty("webdriver.chrome.driver", "D:\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = new InternetExplorerDriver();
        driver.manage().window().maximize();
		
		return driver;
	}
	
	
	public static DriverInit getInstance() {
		if(instanceDriver == null)
			 instanceDriver = new DriverInit();
		
		return instanceDriver;		
	}

}
