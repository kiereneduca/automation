package controllers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.*;

public class PageManager {
	WebDriver driver;
	WebDriverWait wait;
	
	//initialise pages
	Login login;
	MessageBoard messageBoard;
	MessageBoardCreate messageBoardCreate;
	CentreInfo aboutUs;
	
	FormActive formActive;
	FormTemplate formTemplate;
	FormArchive formArchive;
	FormCreate formCreate;
	FormCreateTemplate formCreateTemplate;
	
	PlanCreate planCreate;
	PlanActive planActive;
	PlanView planView;
	PlanTemplate planTemplate;
	PlanCreateTemplate planCreateTemplate;
	
	Children children;
	ChildCreate childCreate;
	LearningStory learningStory;
	GroupStory groupStory;
	GroupStoryView groupStoryView;
	ChildView childView;
	LearningStoryView learningStoryView;
	PendingStories pendingStories;
	
	Routines routines;
	RoutinesCreate routinesCreate;
	
	Activate activate;
	
	public PageManager(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
	}
	
	public Login createLogin() {
		if(login == null) 
			login = new Login(driver, wait);
		return login;
	}
	
	public MessageBoard createMessageBoard() {
		if(messageBoard == null)
			messageBoard = new MessageBoard(driver, wait);
		return messageBoard;
	}
	
	public MessageBoardCreate createMessageBoardCreate() {
		if(messageBoardCreate == null)
			messageBoardCreate = new MessageBoardCreate(driver, wait);
		return messageBoardCreate;
	}
	
	public CentreInfo createAboutUs() {
		if(aboutUs == null)
			aboutUs = new CentreInfo(driver, wait);
		return aboutUs;
	}
	
	
	public FormActive createFormActive() {
		if(formActive == null)
			formActive = new FormActive(driver, wait);
		return formActive;
	}
	
	public FormTemplate createFormTemplate() {
		if(formTemplate == null)
			formTemplate = new FormTemplate(driver, wait);
		return formTemplate;
	}
	
	public FormArchive createFormArchive() {
		if(formArchive == null)
			formArchive = new FormArchive(driver, wait);
		return formArchive;
	}
	
	public FormCreate createFormCreate() {
		if(formCreate == null)
			formCreate = new FormCreate(driver, wait);
		return formCreate;
	}
	
	public FormCreateTemplate createFormCreateTemplate() {
		if(formCreateTemplate == null) 
			formCreateTemplate = new FormCreateTemplate(driver, wait);
		return formCreateTemplate;
		
	}
	
	
	
	public PlanCreate createPlanCreate() {
		if(planCreate == null)
			planCreate = new PlanCreate(driver, wait);
		return planCreate;
	}
	
	public PlanActive createPlanActive() {
		if(planActive == null)
			planActive = new PlanActive(driver, wait);
		return planActive;
	}
	
	public PlanView createPlanView() {
		if(planView == null)
			planView = new PlanView(driver,wait);
		return planView;
	}
	
	public PlanTemplate createPlanTemplate() {
		if(planTemplate == null)
			planTemplate = new PlanTemplate(driver,wait);
		return planTemplate;
	}
	
	public PlanCreateTemplate createPlanCreateTemplate() {
		if(planCreateTemplate == null)
			planCreateTemplate = new PlanCreateTemplate(driver,wait);
		return planCreateTemplate;
	}
	
	
	public Children createChildren() {
		if(children == null)
			children = new Children(driver, wait);
		return children;
	}
	
	public ChildCreate createChildCreate() {
		if(childCreate == null)
			childCreate = new ChildCreate(driver, wait);
		return childCreate;
	}
	
	public LearningStory createLearningStory() {
		if(learningStory == null)
			learningStory = new LearningStory(driver, wait);
		return learningStory;
	}
	
	public GroupStory createGroupStory() {
		if(groupStory == null)
			groupStory = new GroupStory(driver, wait);
		return groupStory;
	}
	
	public GroupStoryView createGroupStoryView() {
		if(groupStoryView == null)
			groupStoryView = new GroupStoryView(driver, wait);
		return groupStoryView;
	}
	
	public LearningStoryView createLearningStoryView() {
		if(learningStoryView == null)
			learningStoryView = new LearningStoryView(driver, wait);
		return learningStoryView;
	}
	
	public ChildView createChildView() {
		if(childView == null)
			childView = new ChildView(driver, wait);
		return childView;
	}
	
	
	public Routines createRoutines() {
		if(routines == null)
			routines = new Routines(driver, wait);
		return routines;
	}
	
	public RoutinesCreate createRoutinesCreate() {
		if(routinesCreate == null)
			routinesCreate = new RoutinesCreate(driver, wait);
		return routinesCreate;
	}

	public Activate createActivate() {
		if(activate == null)
			activate = new Activate(driver, wait);
		return activate;
	}
	
	public PendingStories createPendingStories() {
		if(pendingStories == null)
			pendingStories = new PendingStories(driver, wait);
		return pendingStories;
	}
}
