package testcases;

import pages.*;
import utilities.*;
import controllers.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public class StoryTest {
	WebDriver driver;
	WebDriverWait wait;
	Helper random;
	
	FileManipulation files;
	JavascriptExecutor js;
	
	PageManager pageManager;
	
	Login login;
	MessageBoard messageBoard;
	Children children;
	ChildView childView;
	GroupStory groupStory;
	LearningStory learningStory;
	GroupStoryView groupStoryView;
	LearningStoryView learningStoryView;
	
	String content = "";
	
	
	@BeforeTest
	public void  testSetup() throws IOException {
		DriverInit instance = DriverInit.getInstance();
		driver = instance.chrome();
		wait = new WebDriverWait(driver, 10);
		random = new Helper(driver, wait);
		
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		
		files = new FileManipulation();
		driver.get(files.lookup("littleones"));
		
		js = (JavascriptExecutor) driver;
		
		//initialise pages
		pageManager = new PageManager(driver, wait);
		
		login = pageManager.createLogin();
		messageBoard = pageManager.createMessageBoard();
		children = pageManager.createChildren();
		childView = pageManager.createChildView();
		groupStory = pageManager.createGroupStory();
		learningStory = pageManager.createLearningStory();
		groupStoryView = pageManager.createGroupStoryView();
		learningStoryView = pageManager.createLearningStoryView();
	}
	
	//@Test
	public void learningStoryTest() throws IOException, InterruptedException {
		login.login("bighq_u", "bighq_p");
		messageBoard.clickChildren();
		messageBoard.clickAllChildren();
		List<WebElement> classes = driver.findElements(By.id("summaryContent"));
		for(int i = 0; i < classes.size(); i++) {
			List<WebElement> kids = classes.get(i).findElements(By.xpath("./div"));
			for(int j = 0; j < random.number(1, kids.size()); j++) {
				//get random child and click create story
				kids.get(random.number(1, kids.size() - 1)).findElement(By.xpath(".//a[contains(text(), 'Create Story')]")).click();
				createStory(Integer.toString(j));
			}
		}
	}
	
	@Test
	public void learningStoryTest2() throws IOException, InterruptedException {
		login.login("hq_teacher_u", "hq_teacher_p");
		messageBoard.clickChildren();
		messageBoard.clickAllChildren();
		content = files.readWordFile("storyContent"); //parse Word file once
		
		children.clickFirstChild(); //open first child's profile page
		for (int j=1; j<= Integer.parseInt(files.lookup("loops")); j++) {
			createStory(Integer.toString(j));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	
	public void createStory(String count) throws IOException, InterruptedException {
		childView.createStory();
		learningStory.storyTitle(LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm"))+" " 
				+LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+" - "+count);
		//random.addMedia("video");
		//random.addMedia("PDF");
		//random.addMedia("photo");
		learningStory.writeStory(content);
		//learningStory.learningHappening(content);
		//learningStory.whatNext(content);
		learningStory.addAuthorComment("comment for reviewer test");
		learningStory.publishStory();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	//@Test
	public void groupStoryTest() throws IOException, InterruptedException {
		login.login("admin", "password");
		messageBoard.clickChildren();
		messageBoard.clickAllChildren();
		children.clickGroupStory();
		content = files.readWordFile("storyContent"); //parse Word file once
		
		for (int i=1; i<= Integer.parseInt(files.lookup("loops")); i++) {
			groupStory.selectClass(files.lookup("classes"));
			groupStory.selectChildren(random.numList(0, Integer.parseInt(files.lookup("childUpper"))));
			groupStory.enterTitle(LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm"))+" " 
					+LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+" - "+i+" (Group story)");
			random.addMedia("video");
			random.addMedia("PDF");
			random.addMedia("photo");
			groupStory.enterStory(content);
			groupStory.learningHappening(content);
			groupStory.whatNext(content);
			groupStory.publishStory();
			groupStoryView.clickGroupStory();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	
	@AfterTest
	public void close() {
		driver.close();
	}
	
	
}