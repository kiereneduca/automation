package testcases;

import org.openqa.selenium.*;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import controllers.*;
import pages.*;
import utilities.*;
import org.testng.annotations.*;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class PlanTemplatesTest {
	
	WebDriver driver;
	WebDriverWait wait;
	EventFiringWebDriver eventDriver;
	EventHandler handler;
	PageManager pageManager;
	JavascriptExecutor js;
	FileManipulation files;
	Helper helper;
	
	Login login;
	MessageBoard messageBoard;
	PlanActive planActive;
	PlanTemplate planTemplate;
	PlanCreateTemplate planCreateTemplate;
	PlanView planView;
	
	@BeforeTest
	public void plansSetup() throws IOException {
		DriverInit instance = DriverInit.getInstance();
		driver = instance.chrome();
		wait = new WebDriverWait(driver, 120);
		helper = new Helper(driver, wait);
		
		//Create handler to process console logs on each page/action
		eventDriver = new EventFiringWebDriver(driver);
		handler = new EventHandler();
		eventDriver.register(handler);
		eventDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Create config file reader and set the webpage to NZ test site
		files = new FileManipulation();
		eventDriver.get(files.lookup("bighq"));
		
		js = (JavascriptExecutor) eventDriver;
		
		//initialise pages
		pageManager = new PageManager(eventDriver, wait);
		
		login = pageManager.createLogin();
		messageBoard = pageManager.createMessageBoard();
		
		planActive = pageManager.createPlanActive();
		planTemplate = pageManager.createPlanTemplate();
		planCreateTemplate = pageManager.createPlanCreateTemplate();
		planView = pageManager.createPlanView();
	}
	
	//@Test
	public void createPlanTemplateTest() throws IOException, InterruptedException {
		login.login("bighq_u", "bighq_p");
		messageBoard.clickTools();
		messageBoard.clickPlanning();
		planActive.clickTemplates();
		
		for (int i=0; i< Integer.parseInt(files.lookup("loops")); i++) {
			planTemplate.createTemplate();
			createPlanTemplate(Integer.toString(i));
		}
	}

	public void createPlanTemplate(String text) throws IOException, InterruptedException {
		planCreateTemplate.enterTitle(text + " " + helper.title("titles"));
		
		for (int i=0; i<5; i++) {
			Thread.sleep(1000);
			planCreateTemplate.addField(helper.title("titles"),"planDesc");
		}
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Thread.sleep(1000);
		planCreateTemplate.publishPlan();
	}
	
	
	@Test
	public void copyPlanTemplateTest() throws IOException, InterruptedException {
		login.login("bighq_u", "bighq_p");
		messageBoard.clickTools();
		messageBoard.clickPlanning();
		planActive.clickTemplates();
		
		for (int i=0; i<10; i++) {
			Thread.sleep(1000);
			planTemplate.copyTemplate();
			copyPlanTemplate();
		}
	}

	private void copyPlanTemplate() throws InterruptedException {
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		planCreateTemplate.publishPlan();
	}

	
	@AfterTest
	public void testClose() {
		eventDriver.close();
	}
}