package testcases;

 import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import controllers.DriverInit;
import controllers.PageManager;
import utilities.*;


import pages.*;

public class SitePopulate {
	WebDriver driver;
	WebDriverWait wait;
	PageManager pageManager;
	JavascriptExecutor js;
	FileManipulation files;
	Helper random;
	
	Login login;
	MessageBoard messageBoard;
	CentreInfo aboutUs;
	
	Children children;
	ChildCreate childCreate;
	LearningStory learningStory;
	GroupStory groupStory;
	GroupStoryView groupStoryView;
	ChildView childView;
	
	PlanActive planActive;
	PlanCreate planCreate;
	PlanView planView;
	
	FormActive formActive;
	FormCreate formCreate;
	FormTemplate formTemplate;
	FormCreateTemplate formCreateTemplate;
	
	String content = "";
	
	@BeforeTest
	public void populateSetup() throws IOException {
		DriverInit instance = DriverInit.getInstance();
		driver = instance.chrome();
		wait = new WebDriverWait(driver, 10);
		random = new Helper(driver, wait);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Create config file reader and set the webpage to designated site
		files = new FileManipulation();
		driver.get(files.lookup("bignz"));
		
		js = (JavascriptExecutor) driver;
		
		//initialise pages
		pageManager = new PageManager(driver, wait);
		
		login = pageManager.createLogin();
		messageBoard = pageManager.createMessageBoard();
		aboutUs = pageManager.createAboutUs();
		
		children = pageManager.createChildren();
		childCreate = pageManager.createChildCreate();
		learningStory = pageManager.createLearningStory();
		groupStory = pageManager.createGroupStory();
		groupStoryView = pageManager.createGroupStoryView();
		childView = pageManager.createChildView();
		
		planActive = pageManager.createPlanActive();
		planCreate = pageManager.createPlanCreate();
		planView = pageManager.createPlanView();
		
		formActive = pageManager.createFormActive();
		formCreate = pageManager.createFormCreate();
		formTemplate = pageManager.createFormTemplate();
		formCreateTemplate = pageManager.createFormCreateTemplate();
		
	}
	
	//START TEST CASES
	
	//----- WORKING ----- 
//	@Test
	public void childPopulate() throws IOException, InterruptedException {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		login.login("bignz_u", "bignz_p");
		messageBoard.clickChildren();
		messageBoard.clickAllChildren();
		for(int i = 0; i < Integer.parseInt(files.lookup("loops")); i++) {
			children.createChild();
			createChild(i);
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
	}
	
//	@Test
	public void storyPopulate() throws IOException, InterruptedException {
		login.login("bignz_u", "bignz_p");
		content = files.readWordFile("storyContent"); //parse Word file once
		for(int k = 0; k < Integer.parseInt(files.lookup("loops")); k++) {
			messageBoard.clickChildren();
			messageBoard.clickAllChildren();
			//get random child and click create story
			List<WebElement> kids = driver.findElements(By.className("educa-profile-container"));
			kids.get(random.number(1, kids.size() - 1)).findElement(By.xpath(".//a[contains(text(), 'Create Story')]")).click();
			createStory(Integer.toString(k));
			Thread.sleep(1000);
		}
	}
	
	//----- WORKING ----- (Changed Recently)
//	@Test
	public void groupStoryPopulate() throws IOException, InterruptedException {
		login.login("admin", "password");
		content = files.readWordFile("storyContent"); //parse Word file once
		messageBoard.clickChildren();
		messageBoard.clickAllChildren();
		children.clickGroupStory();
		for(int i = 0; i < Integer.parseInt(files.lookup("loops")); i++) {
			if(i > 0)
				groupStoryView.createGroupStory();
			createGroupStory();
		}
		groupStoryView.logout();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	//@Test
	public void planPopulate() throws IOException {
		login.login("admin", "password");
		messageBoard.clickAboutUs();
		aboutUs.clickPlanning();
		planActive.createPlan();
		for(int i = 0; i < Integer.parseInt(files.lookup("loops")); i++) {
			createPlan();
		}
		
		planActive.logout();
		
	}
//	@Test	
	public void formPopulate() throws IOException {
		login.login("admin", "password");
		messageBoard.clickAboutUs();
		aboutUs.clickForms();
		for(int i = 0; i < Integer.parseInt(files.lookup("loops")); i++) {
			formActive.createForm();
		}
		
		
	}
	
	//END TEST CASES
	
	
	//Creation helper methods
	
	public void createChild(int i) throws InterruptedException, IOException {
		childCreate.enterName("Child", "" + i);
		childCreate.selectGender();
		childCreate.dateOfBirth(random.date(random.number(2009, 2016)));
		childCreate.startDate(random.date(Integer.parseInt(Year.now().toString())));
		childCreate.pickClass(random.number(1, childCreate.getClasses()));
		childCreate.saveChild();
		childView.returnToChildren();
	}
	
	public void createStory(String count) throws IOException, InterruptedException {
		learningStory.storyTitle(LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm"))+" " 
				+LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+" - "+count);
		groupStory.enterDate(random.date(Integer.parseInt(files.lookup("thisYear"))));
		//random.addMedia("video");
		//random.addMedia("PDF");
		//random.addMedia("photo");
		//learningStory.writeStory(content);
		//learningStory.learningHappening(content);
		//learningStory.whatNext(content);
		//learningStory.curriculumGoals("Animal Recognition");
		learningStory.linkPlan();
		learningStory.publishStory();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	public void createGroupStory() throws IOException, InterruptedException {
		groupStory.selectClass(files.lookup("classes"));
		groupStory.selectChildren(random.numList(Integer.parseInt(files.lookup("childLower")), Integer.parseInt(files.lookup("childUpper"))));
		//groupStory.enterTitle(random.title(files.readWordFile("titles")));
		groupStory.enterTitle(LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm"))+" " 
				+LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+" (Group story)");
		groupStory.enterDate(random.date(Integer.parseInt(files.lookup("thisYear"))));
		//random.addMedia("video");
		//random.addMedia("PDF");
		//random.addMedia("photo");
		groupStory.enterStory(content);
		//groupStory.learningHappening(content);
		//groupStory.whatNext(content);
		groupStory.publishStory();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public void createPlan() throws IOException {
		planCreate.selectTemplate("planName");
		if(driver.findElements(By.xpath("//div[contains(text(), 'Link To Children')]")).size() > 0)
			planCreate.linkToTeachers(random.thing(files.lookup("teachers")));
		if(driver.findElements(By.xpath("//div[contains(text(), 'Link To Teachers')]")).size() > 0)
			planCreate.linkToChildren(random.numList(Integer.parseInt(files.lookup("childLower")), Integer.parseInt(files.lookup("childUpper"))));
		planCreate.enterTitle(random.title("titles"));
		//planCreate.enterSubject(files.readWordFile("summary"));
		planCreate.enterDescription(files.readWordFile("planContent"));
		if(driver.findElements(By.xpath("//div[contains(@data-ng-repeat, 'editPlan.template.customfields')]")).size() > 0) {
			List<WebElement> custom = driver.findElements(By.xpath("//div[contains(@data-ng-repeat, 'editPlan.template.customfields')]"));
			for(int i = 0; i <= custom.size(); i++) {
				driver.switchTo().frame(custom.get(i).findElement(By.xpath(".//iframe")));
				driver.findElement(By.id("tinymce")).sendKeys(files.readWordFile("planCustom"));
				driver.switchTo().defaultContent();
			}
		}
		//curriculum adding if necessary
		//adding resources
		planCreate.linkToPlans(files.lookup("planLinkName"));
		planCreate.publishPlan();
		planView.viewAllPlans();
				
	}
	
	public void createForm() throws IOException {
		formCreate.selectForm(files.lookup("formName"));
		formCreate.chooseFormPerson(random.thing(files.lookup("teachers")));
		formCreate.addTitle(random.title(files.readWordFile("titles")));
		formCreate.addSummary(files.readWordFile("summary"));
		String answers = files.readWordFile("answers");
		for (int i = 0; i <= formCreate.getSections() - 1; i++) {
			formCreate.switchSection(i);
			formCreate.answerQuestions(random.thing(answers));
		}
		formCreate.publishForm();
	}
	
	@AfterTest
	public void testClose() {
		driver.close();
	}
}
