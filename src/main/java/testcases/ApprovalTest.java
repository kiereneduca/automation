package testcases;

import pages.*;
import utilities.*;
import controllers.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public class ApprovalTest {
	WebDriver driver;
	WebDriverWait wait;
	Helper random;
	
	FileManipulation files;
	JavascriptExecutor js;
	
	PageManager pageManager;
	
	Login login;
	MessageBoard messageBoard;
	PendingStories pendingStories;
	LearningStoryView learningStoryView;
	
	String content = "";
	
	
	@BeforeTest
	public void  testSetup() throws IOException {
		DriverInit instance = DriverInit.getInstance();
		driver = instance.chrome();
		wait = new WebDriverWait(driver, 10);
		random = new Helper(driver, wait);
		
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		
		files = new FileManipulation();
		driver.get(files.lookup("littleones"));
		
		js = (JavascriptExecutor) driver;
		
		//initialise pages
		pageManager = new PageManager(driver, wait);
		
		login = pageManager.createLogin();
		messageBoard = pageManager.createMessageBoard();
		pendingStories = pageManager.createPendingStories();
		learningStoryView = pageManager.createLearningStoryView();
	}
	
	
	@Test
	public void approveStoryTest() throws IOException, InterruptedException {
		login.login("admin", "password");
		messageBoard.clickChildren();
		messageBoard.clickPendingStories();
		
		for (int j=1; j<= Integer.parseInt(files.lookup("loops")); j++) {
			approveStory(Integer.toString(j));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	
	public void approveStory(String count) throws IOException, InterruptedException {
		pendingStories.approveStory();
		pendingStories.addComment("approval comment");
		pendingStories.clickApprove();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	
	@AfterTest
	public void close() {
		driver.close();
	}
	
	
}