package testcases;

import pages.*;
import utilities.*;
import controllers.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public class RoutinesTest {
	WebDriver driver;
	WebDriverWait wait;
	Helper random;
	
	FileManipulation files;
	JavascriptExecutor js;
	
	PageManager pageManager;
	
	Login login;
	MessageBoard messageBoard;
	Routines routines;
	RoutinesCreate routinesCreate;
	
	String content = "";
	
	
	@BeforeTest
	public void  testSetup() throws IOException {
		DriverInit instance = DriverInit.getInstance();
		driver = instance.chrome();
		wait = new WebDriverWait(driver, 10);
		random = new Helper(driver, wait);
		
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		
		files = new FileManipulation();
		driver.get(files.lookup("bighq"));
		
		js = (JavascriptExecutor) driver;
		
		//initialise pages
		pageManager = new PageManager(driver, wait);
		
		login = pageManager.createLogin();
		messageBoard = pageManager.createMessageBoard();
		routines = pageManager.createRoutines();
		routinesCreate = pageManager.createRoutinesCreate();
	}
	
	@Test
	public void routinesPopulate() throws IOException {
		//login
		login.login("bighq_u", "bighq_p");
				
		//navigate to create routines Page
		messageBoard.clickTools();
		messageBoard.clickRoutines();
		
		for (int i = 0; i <= Integer.parseInt(files.lookup("loops")); i++) {
			createRoutine(Integer.toString(i));
		}
	}
	
	public void createRoutine(String count) throws NumberFormatException, IOException {
		routines.createRoutine();
		routinesCreate.selectRandomType(random.number(0, 4), random.numList(0, Integer.parseInt(files.lookup("childUpper"))));
		//routinesCreate.selectSleepType(random.numList(0, Integer.parseInt(files.lookup("childUpper"))));
		routinesCreate.clickSave();
	}
	
	@AfterTest
	public void close() {
		driver.close();
	}
	
	
}