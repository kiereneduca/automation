package testcases;

import org.openqa.selenium.*;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import controllers.*;
import pages.*;
import utilities.*;
import org.testng.annotations.*;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class PlansTest {
	
	WebDriver driver;
	WebDriverWait wait;
	EventFiringWebDriver eventDriver;
	EventHandler handler;
	PageManager pageManager;
	JavascriptExecutor js;
	FileManipulation files;
	Helper random;
	
	Login login;
	MessageBoard messageBoard;
	CentreInfo aboutUs;
	
	PlanActive planActive;
	PlanCreate planCreate;
	PlanView planView;
	
	@BeforeTest
	public void plansSetup() throws IOException {
		DriverInit instance = DriverInit.getInstance();
		driver = instance.chrome();
		wait = new WebDriverWait(driver, 15);
		random = new Helper(driver, wait);
		
		//Create handler to process console logs on each page/action
		eventDriver = new EventFiringWebDriver(driver);
		handler = new EventHandler();
		eventDriver.register(handler);
		eventDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Create config file reader and set the webpage to NZ test site
		files = new FileManipulation();
		eventDriver.get(files.lookup("bighq"));
		
		js = (JavascriptExecutor) driver;
		
		//initialise pages
		pageManager = new PageManager(eventDriver, wait);
		
		login = pageManager.createLogin();
		messageBoard = pageManager.createMessageBoard();
		planActive = pageManager.createPlanActive();
		planCreate = pageManager.createPlanCreate();
		planView = pageManager.createPlanView();
	}
	
	@Test
	public void createPlanTest() throws IOException, InterruptedException {
		login.login("bighq_u", "bighq_p");
		messageBoard.clickTools();
		messageBoard.clickPlanning();
		planActive.createPlan();
		createPlan();
		
	}
	
	public void createPlan() throws IOException, InterruptedException {
		planCreate.selectTemplate("Educa Group Plan"); //
		/*for (int i=1; i<= Integer.parseInt(files.lookup("loops")); i++) {
			//link random children
			planCreate.linkToChildren(random.numList(0, Integer.parseInt(files.lookup("childUpper"))));
		}*/
		planCreate.linkAllChildren();
		planCreate.linkAllTeachers();
		
		planCreate.enterTitle(random.title("titles"));
		planCreate.enterSubject(files.readWordFile("summary"));
		planCreate.enterDescription("planContent");
		if(eventDriver.findElements(By.xpath("//div[contains(@data-ng-repeat, 'editPlan.template.customfields')]")).size() > 0) {
			List<WebElement> custom = eventDriver.findElements(By.xpath("//div[contains(@data-ng-repeat, 'editPlan.template.customfields')]"));
			for(int i = 0; i <= custom.size(); i++) {
				eventDriver.switchTo().frame(custom.get(i).findElement(By.xpath(".//iframe")));
				eventDriver.findElement(By.id("tinymce")).sendKeys(files.lookup("planCustom"));
			}
		}
		//curriculum adding if necessary
		//adding resources
		//linking to other plans
		planCreate.publishPlan();
		planView.viewAllPlans();
	}
	
	@AfterTest
	public void testClose() {
		eventDriver.close();
	}
}