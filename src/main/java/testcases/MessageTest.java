package testcases;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import controllers.*;
import utilities.*;
import pages.*;

public class MessageTest {
	WebDriver driver;
	WebDriverWait wait;
	Helper random;
	
	FileManipulation files;
	
	
	//pages involved
	PageManager pageManager;
	
	Login login;
	MessageBoard messageBoard;
	MessageBoardCreate messageBoardCreate;
	
	@BeforeTest
	public void testSetup() throws IOException {
		DriverInit instance = DriverInit.getInstance();
		driver = instance.chrome();
		wait = new WebDriverWait(driver, 10);
		random = new Helper(driver, wait);
		
		
		files = new FileManipulation();
		
		pageManager = new PageManager(driver, wait);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(files.lookup("bighq"));
		//driver.get("https://kierentest.educa.co.nz/");
		
		//initialise pages
		login = pageManager.createLogin();
		messageBoard = pageManager.createMessageBoard();
		messageBoardCreate = pageManager.createMessageBoardCreate();
	}
	
	@Test
	public void messageTest() throws IOException, InterruptedException {
		login.login("bighq_u", "bighq_p");
		String content = files.readWordFile("storyContent");
		for(int i = 0; i < Integer.parseInt(files.lookup("loops")); i++) {
			messageBoard.createMessage();
			createMessage(content, Integer.toString(i));
		}
	}
	
	public void createMessage(String content, String count) throws IOException, InterruptedException {
		messageBoardCreate.addTitle(LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm"))+" " 
				+LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+" - "+count+"(Message)");
		//messageBoardCreate.addDate(random.date(Integer.parseInt(files.lookup("thisYear"))));
		//random.addMedia("video");
		//random.addMedia("PDF");
		//random.addMedia("photo");
		messageBoardCreate.writeMessage(content);
		messageBoardCreate.publishMessage();
	}
	
	@AfterTest
	public void close() {
		driver.close();
	}
}
