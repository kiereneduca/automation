package testcases;

import pages.*;
import utilities.*;
import controllers.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public class ActivateUsersTest {
	WebDriver driver;
	WebDriverWait wait;
	Helper random;
	
	FileManipulation files;
	JavascriptExecutor js;
	
	PageManager pageManager;
	
	Login login;
	Activate activate;
	MessageBoard messageBoard;
	
	
	@BeforeTest
	public void  testSetup() throws IOException {
		DriverInit instance = DriverInit.getInstance();
		driver = instance.chrome();
		wait = new WebDriverWait(driver, 10);
		random = new Helper(driver, wait);
		
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		
		files = new FileManipulation();
		driver.get(files.lookup("bignz"));
		
		js = (JavascriptExecutor) driver;
		
		//initialise pages
		pageManager = new PageManager(driver, wait);
		
		login = pageManager.createLogin();
		activate = pageManager.createActivate();
		messageBoard = pageManager.createMessageBoard();
	}

	@Test
	public void readData() throws IOException, InterruptedException {
		BufferedReader br = new BufferedReader(new FileReader(files.lookup("students")));
		Scanner sc = new Scanner(new File(files.lookup("students"))); //Change file type
		sc.useDelimiter(Pattern.compile(","));
		
		List<String> users = new ArrayList<String>();
		String line = "";
		sc.next(); //skip [
		while(sc.hasNext()) {
			line = sc.next();
			users.add(line);
		}
		br.close();
		//Remove the ] from the last element
		String lastElement = users.get(users.size()-1);
		lastElement = lastElement.substring(0, lastElement.length() - 1);
		users.set(users.size()-1, lastElement);
		
		acceptParentInvites(users); //Change to teacher/parent/student method
	}
	
	public void acceptTeacherInvites(List<String> users) throws IOException, InterruptedException {
		
	}
	
	public void acceptParentInvites(List<String> users) throws IOException, InterruptedException {
		int current = 0;
		while (current < 47) {
			users.remove(0);
			current++;
		}
		for (String user: users) {
			current++;
			System.out.println("Activating user: "+current);
			System.out.println("User: "+user);
			user = user.substring(1, user.length()-1); //Remove ""
			driver.get(user);
			activate.activateParent("geteduca", "geteduca");
			activate.logout();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	
	public void acceptStudentInvites(List<String> users) throws IOException, InterruptedException {
		
	}
	
	@AfterTest
	public void close() {
		driver.close();
	}
	
	
}