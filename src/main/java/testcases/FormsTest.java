package testcases;

import org.openqa.selenium.*;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;

import controllers.*;
import pages.*;
import utilities.*;
import org.testng.annotations.*;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;


public class FormsTest {
	
	WebDriver driver;
	WebDriverWait wait;
	EventFiringWebDriver eventDriver;
	EventHandler handler;
	
	PageManager pageManager;
	JavascriptExecutor js;
	FileManipulation files;
			
	Helper helper;
	
	MessageBoard messageBoard;
	FormActive formActive;
	FormTemplate formTemplate;
	FormCreateTemplate formCreateTemplate;
	Login login;
	FormCreate formCreate;
	
	@BeforeTest
	public void testSetup() throws IOException {
		//Create web driver instance (can use chrome/other browsers if necessary)
		DriverInit instance = DriverInit.getInstance();
		driver = instance.chrome();
		wait = new WebDriverWait(driver, 15);
		helper = new Helper(driver, wait);
		
		//Create handler to process console logs on each page/action
		eventDriver = new EventFiringWebDriver(driver);
		handler = new EventHandler();
		eventDriver.register(handler);
		
		//Setting timeout period to 10 seconds - needed for webpage changes
		eventDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Create config file reader and set the webpage to NZ test site
		files = new FileManipulation();
		eventDriver.get(files.lookup("littleones"));
		
		js = (JavascriptExecutor) eventDriver;
		//initialise pages
		pageManager = new PageManager(eventDriver, wait);
		
		login = pageManager.createLogin();
		messageBoard = pageManager.createMessageBoard();
		
		formActive = pageManager.createFormActive();
		formCreate = pageManager.createFormCreate();
		formTemplate = pageManager.createFormTemplate();
		formCreateTemplate = pageManager.createFormCreateTemplate();
		
	}
	
	@Test
	public void templateTest() throws IOException, InterruptedException {
		//login
		login.login("admin", "password");
		
		//navigate to create form Page
		messageBoard.clickTools();
		messageBoard.clickForms();
		//external method to create form template
		for (String s : files.lookup("formTypes").split(",")) {
			formActive.createTemplate();
			createTemplate(s);
		}
		formTemplate.messageBoard();
		messageBoard.clickLogout();
			
	}
	
	public void createTemplate(String formType) throws IOException, InterruptedException {
		eventDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		//Input Basic information
		formCreateTemplate.changeFormType(formType);
		formCreateTemplate.addTitle(helper.title("titles"));
		eventDriver.switchTo().frame("summary_ifr");
		formCreateTemplate.addDescription("This is an automatically created test form.");
		eventDriver.switchTo().defaultContent();
		Thread.sleep(1000);
		
		String [] questions = files.readWordFile("formQuestions").split(",");
		String [] questionTypes = files.lookup("formQuestionTypes").split(",");
		//Loop Through 4 sections as per Acceptance Criteria
		for(int i = 0; i < 4; i++) {
			formCreateTemplate.addSection(helper.title("titles"), files.readWordFile("formSectionDesc"));
			Thread.sleep(2000);
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			
			//One of each question type per section
			for(String s : questionTypes) {
				Thread.sleep(1000);
				formCreateTemplate.newQuestion(questions[(int)(Math.random() * (questions.length - 1))], s);
			}
			formCreateTemplate.addTextBox();
		}
		Thread.sleep(1000);
		formCreateTemplate.publishForm();
		eventDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	//@Test
	public void createFormTest() throws IOException, InterruptedException {
		login.login("admin", "password");
		messageBoard.clickTools();
		messageBoard.clickForms();
		for (int i = 0; i < 20; i++) {
			formActive.createForm();
			createForm(Integer.toString(i));
		}		
	}
	
	public void createForm(String count) throws IOException, InterruptedException {
		eventDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		formCreate.selectForm(files.lookup("formName"));
		//formCreate.chooseFormPerson(files.lookup("WITFFChild"));
		formCreate.addTitle(count + helper.title("titles"));
		formCreate.addSummary(files.readWordFile("formSummary"));
		for (int i = 0; i <= formCreate.getSections() - 1; i++) {
			formCreate.switchSection(i);
			formCreate.answerQuestions(helper.thing("answers"));
		}
		eventDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		formCreate.publishForm();
		//formActive.messageBoard();
		//messageBoard.clickLogout();
	}
	
	
	@AfterTest
	public void testClose() {
		eventDriver.close();
	}
	
}
