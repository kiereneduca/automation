/*
 * 
 * A helper class for calls of random elements of various types
 * All methods static to avoid instancing problems
 * 
 * 
 */


package utilities;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helper {
	
	WebDriver driver;
	WebDriverWait wait;
	
	Random random;
	FileManipulation files;

	
	public Helper(WebDriver driver, WebDriverWait wait) {
		random = new Random();
		files = new FileManipulation();
		this.driver = driver;
		this.wait = wait;
	}
	
	
	/*
	 * 
	 * 		Random return methods	
	 * 
	 */
	
	//random date- random date on specified year, always before current date.
	public String date(int chosenYear) {
		
		int day = 0, month = 0;
		LocalDate date = LocalDate.now();
		
		if(chosenYear == date.getYear())
			month = (random.nextInt(date.getMonthValue())) + 1;
		else
			month = (random.nextInt(12)) + 1;
		
		switch(month) {
			case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				day = (random.nextInt(31)) + 1;
				break;
			case 2: 
				day = (random.nextInt(28)) + 1;
				break;
			case 4: case 6: case 9: case 11:
				day = (random.nextInt(30)) + 1;
				break;
		}
		if(month == date.getMonthValue())
			day = (random.nextInt(date.getDayOfMonth())) + 1;

		return "" + day + "/" + month + "/" + chosenYear;
		
	}
	
	//random title using two random words from a file
	public String title(String configTitle) throws IOException {
		String [] titles = files.readWordFile(configTitle).split(",");
		return "" + titles[random.nextInt(titles.length)] + " " + titles[random.nextInt(titles.length)];
	}
	
	public String name(String firstName, String lastName) throws IOException {
		String [] firstNames = files.readWordFile(firstName).split(",");
		String [] lastNames = files.readWordFile(lastName).split(","); 
		return firstNames[random.nextInt(firstNames.length)] + " " + lastNames[random.nextInt(lastNames.length)];
	}
	
	//takes a list of things delimited with a comma and returns a random choice
	public String thing(String thing) {
		String [] things= thing.split(",");
		return things[random.nextInt(things.length)];
	}
	
	//Reutns a random number within given bounds
	public int number(int lower, int upper) {
		//get range independent of order of upper/lower
		int range = Math.abs(upper - lower);
		return random.nextInt(range + 1) + (lower <= upper ? lower : upper);
	}
	
	//Returns a somewhat random list of integers in a given bound, no replacements
	public int[] numList(int lower, int upper) {
		return new Random().ints(lower, upper + 1).distinct().limit(random.nextInt(upper)).toArray();
		
	}
	
	/* 
	 * 
	 * Upload Helper Methods
	 * 
	 */
	
	public void addMedia(String mediaType) throws IOException, InterruptedException {
		switch(mediaType) {
			case "video":
				wait.until(ExpectedConditions.elementToBeClickable(By.id("story_video_upload"))).click();
				uploadVideo();
				break;
			case "PDF":
				wait.until(ExpectedConditions.elementToBeClickable(By.id("story_file_upload_iframe"))).click();
				uploadPDF();
				break;
			case "photo":
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), 'Add Photos')]"))).click();
				uploadPhoto();
				break;
			default:
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(), 'Add Resources') OR contains(text(), 'Upload Files')]"))).click();
				uploadMisc();
				break;
			
		}
	}
	
	public void uploadVideo() throws IOException, InterruptedException {
		Thread.sleep(1000);
		Runtime.getRuntime().exec("C:\\Users\\Kieren Swann\\Documents\\Work\\Educa\\AutoIT\\sampleVideo.exe");
	}
	
	public void uploadPDF() throws IOException, InterruptedException {
		Thread.sleep(1000);
		//clicking Add PDF sometimes doesn't bring up the Local File/Drive modal...
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), 'Local File')]"))).click();
		Thread.sleep(1000);
		Runtime.getRuntime().exec("C:\\Users\\Kieren Swann\\Documents\\Work\\Educa\\AutoIT\\samplePDF.exe");
	}
	
	public void uploadPhoto() throws IOException, InterruptedException {
		driver.switchTo().frame("photoUploadIframe");
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"fileupload\"]/div[1]/div[1]/span"))).click();
		Thread.sleep(1000);
		Runtime.getRuntime().exec("C:\\Users\\Kieren Swann\\Documents\\Work\\Educa\\AutoIT\\sampleImage.exe");		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(@class, 'btn-danger')]")));
		wait.until(ExpectedConditions.elementToBeClickable(By.id("close_multi_photo_upload"))).click();
		driver.switchTo().defaultContent();
	}

	public void uploadMisc() throws IOException {
		driver.switchTo().frame("fileUploadIframe");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@class, 'fileinput-button')]"))).click();
		Runtime.getRuntime().exec("videoPath");
		wait.until(ExpectedConditions.invisibilityOf((WebElement) By.xpath("//button[contains(@class, 'btn-danger')]")));
		wait.until(ExpectedConditions.elementToBeClickable(By.id("saveresources"))).click();
		driver.switchTo().defaultContent();
	}
	
	/*
	 *  Curriculum choice and & Uploader methods
	 * 
	 */
	
	public void curriculum(String curriculum) throws IOException {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), '" + curriculum + "' )]"))).click();
		for(int i = 1; i < 4; i++) {
			String[] goal = files.lookup("goal" + i).split(",");
			boolean firstLoop = true;
			for (String s : goal) {
				if(firstLoop) {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), '" + s + "')]"))).click();
					firstLoop = false;
				} else {
					driver.findElement(By.xpath("//a[contains(text(), '" + s + "')]/../..//label")).click();
				}
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.id("close_curriculum_links_window"))).click();
	}
	
}
