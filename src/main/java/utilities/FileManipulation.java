package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

public class FileManipulation {
	
	public String readWordFile(String filePath) throws IOException {
		
		filePath = getObjectRepository().getProperty(filePath);
		String content = "";
	    try {
	       
	        FileInputStream fis = new FileInputStream(filePath);
	        HWPFDocument doc = new HWPFDocument(fis);
	        WordExtractor we = new WordExtractor(doc);
	        String[] paragraphs = we.getParagraphText();
	        for (String para : paragraphs) {
	            content += para.toString();
	        }
	        fis.close();
	        we.close();
	        return content;
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return content;
	}
	
	 Properties p = new Properties();
	    public Properties getObjectRepository() throws IOException{
	        
	    	//Read object repository file
	        InputStream stream = new FileInputStream(new File(System.getProperty("user.dir")+"\\src\\main\\resources\\configuration.properties"));
	       
	        //load all objects
	        p.load(stream);
	         return p;
	    }
	    
	    public String lookup(String key) throws IOException {
	    	
	    	return getObjectRepository().getProperty(key);
	    }
}
